import character.Human;
import character.Magician;
import character.Monster;
import character.Player;
import java.util.ArrayList;
public class Game{
    ArrayList<Player> player = new ArrayList<Player>();

    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
        for (Player pemain: player) {
            if (pemain.getName().equals(name)){
                return pemain;
            }
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
        if (find(chara) !=null) {
            return "Sudah ada karakter bernama "+chara;
        }
        if ("Human".equals(tipe)) {
            Player human = new Human(chara, hp);
            if (hp==0) human.setDead(true);
            player.add(human);
        }
        else if (tipe.equals("Magician")) {
            Player magician = new Magician(chara, hp);
            if (hp==0) magician.setDead(true);
            player.add(magician);
        }
        else if (tipe.equals("Monster")) {
            Player monster = new Monster(chara,hp);
            if (hp==0) monster.setDead(true);
            player.add(monster);
        }
        return chara +" ditambah ke game";
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
        if (find(chara)!=null) {
            return chara+"sudah ada";
        }
        if (!tipe.equals("Monster")) return "gak bisa";
        Player monster = new Monster(chara, hp, roar);
        if (hp==0) monster.setDead(true);
        player.add(monster);
        return chara +"berhasil dibuat";
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
        Player pemain = find(chara);
        if (pemain != null) {
            String pop = pemain.getName();
            player.remove(pemain);
            return pop + " dihapus dari game";
        }
        return "Tidak ada "+chara;
    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */

    public String status(String chara) {
        Player p1 = find(chara);
        String status = "";
        if (p1 == null){
            return "Pemain tidak ada";
        }
        status += p1.getType()+" "+p1.getName()+"\n";
        status += "HP: " + p1.getHp()+"\n";
        if (!(p1.isDead())) status+= "Masih hidup\n";
        else status += "Sudah meninggal dunia dengan damai\n";
        if (p1.getDiet().size()==0){
            status += "Belum memakan siapa siapa  ";
        }
        else {
            status += "Memakan ";
            for (Player mayat: p1.getDiet() ) {
                status+= mayat.getType()+" "+ mayat.getName()+", ";

            }
        }
        status = status.substring(0,status.length()-2);
        return status;
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
        String willPrinted="";
        if (player.size() == 0) return "Tidak ada pemain";
        for (Player pemain: player) {
            willPrinted+= status(pemain.getName())+"\n";
        }
        return willPrinted;
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
        Player pemain = find(chara);
        String termakan = "";
        if (pemain.getDiet() == null) {
            return "belum memakan siapapun";
        }
        for (int i =0;i< pemain.getDiet().size() ;i++ ) {
            termakan += pemain.getDiet().get(i).getType()+" "+pemain.getDiet().get(i).getName()+", ";
        }
        return termakan.substring(0,termakan.length()-2);
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
        String total="";
        for (Player pemain : player) {
            total+= pemain.getName()+", ";
        }
        return "Termakan : "+ total.substring(0,total.length()-2);
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        Player p1 = find(meName);
        Player p2 = find(enemyName);
        if (p1 == null || p2 == null) {
            return "tidak bisa";
        }
        else {
            p1.attack(p2);
        }
        return "Nyawa "+ enemyName+ " "+p2.getHp();
    }


     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
        Player p1 = find(meName);
        Player p2 = find(enemyName);
        if (p1== null) return p1.getName() + " tidak ada";
        if (p2== null) return p2.getName() + " tidak ada";
        int sumDamage = 10 * p2.damageMultiplier;
        if (!(p1.getType().equals("Magician"))) {
            return p1.getName() +" tidak bisa melakukan burn";
        }
        p1.burn(p2);
        if (!(p2.isDead())) return "Nyawa "+enemyName+" "+p2.getHp();
        else return "Nyawa "+enemyName+" 0\n dan matang";
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan @
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
        Player p1 = find(meName);
        Player p2 = find(enemyName);
        if (p1 == null) {
            return "gak ada player 1 nya woy";
        }
        if (p2 == null) {
            return "gak ada yang dimakan";
        }
        if (p1.canEat(p2)) {
            p1.eat(p2);
            player.remove(p2);
            p1.setHP(p1.getHp()+15);
            return meName+" memakan "+enemyName+"\nNyawa "+meName+" kini "+p1.getHp();
        }
        return meName+" tidak bisa memakan "+enemyName;
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
        Player p1 = find(meName);
        if (p1 == null) return "Tidak ada "+meName;
        if (!(p1.getType()).equals("Monster")) {
            return meName + " tidak bisa berteriak";
        }
        return p1.roar();
    }
}
