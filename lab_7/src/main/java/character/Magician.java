package character;
import java.util.ArrayList;
public class Magician extends Human{
    private String name;
    private int hp;
    private ArrayList<Player> diet = new ArrayList<Player>();
    private boolean burned;
    private boolean dead;
    private boolean eaten;
    public final int damageMultiplier=2;
    private final String type = "Magician";

    public Magician(String name, int hp){
        super(name, hp);
    }

    public void burn(Player enemy) {
        if (enemy.isDead()) {
            enemy.setBurned(true);
        }
        else {
            if (enemy.getType().equals("Magician")) {
                if (enemy.getHp()<=20) {
                    enemy.setHP(0);
                    enemy.setDead(true);
                    enemy.setBurned(true);
                }
                else {
                    enemy.setHP(enemy.getHp()-20);
                }
            }
            else {
                if (enemy.getHp()<=10) {
                    enemy.setHP(0);
                    enemy.setDead(true);
                    enemy.setBurned(true);
                }
                else {
                    enemy.setHP(enemy.getHp()-10);
                }
            }
        }
    }

    public String getType(){
        return this.type;
    }
}
//  write Magician Class here
