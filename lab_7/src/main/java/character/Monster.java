package character;
import java.util.ArrayList;

public class Monster extends Player{
	private String name;
	private int hp;
	private ArrayList<Player> diet = new ArrayList<Player>();
	private String roaring;
	private final String type="Monster";

	public Monster(String name, int hp){
		super(name, hp*2);
		this.roaring = "AAAAAAaaaAAAAAaaaAAAAAA";
	}
	public Monster(String name, int hp,String roaring){
		super(name, hp*2, roaring);
	}

	public String roar(){
		return this.roaring;
	}

	public String getType(){
		return this.type;
	}

}
//  write Monster Class here
