package character;
import java.util.ArrayList;

public class Player{
	protected String name;
	protected int hp;
	protected ArrayList<Player> diet = new ArrayList<Player>(0);
	protected boolean burned;
	protected boolean dead;
	protected boolean eaten;
	public final int damageMultiplier=1;
	protected String type;
	protected String roaring;

	public Player(String name, int hp){
		this.name = name;
		this.hp = hp;
	}

	public Player(String name, int hp, String roaring){
		this.name = name;
		this.hp = hp;
		this.roaring = roaring;
	}

	public String roar(){
		return this.roaring;
	}

	public void burn(Player enemy){

	}
	public String getName(){
		return this.name;
	}

	public int getHp(){
		return this.hp;
	}

	public void setHP(int hp){
		this.hp = hp;
	}

	public void eat(Player makanan){
		this.diet.add(makanan);
	}
	public void attack(Player enemy){
		int sumDamage = 10 * enemy.getDamageMultiplier();
		if (enemy.getHp()> sumDamage){
			enemy.setHP(enemy.getHp()-sumDamage);
		}
		else {
			enemy.setHP(0);
			enemy.setDead(true);
		}
	}


	public boolean isBurned(){
		return this.burned;
	}

	public void setBurned(boolean burned){
		this.burned = burned;
	}

	public boolean isDead(){
		return dead;
	}

	public void setDead(boolean dead){
		this.dead = dead;
	}

	public boolean isEaten(){
		return eaten;
	}

	public void setEaten(boolean eaten){
		this.eaten = eaten;
	}

	public int getDamageMultiplier(){
		return damageMultiplier;
	}

	public ArrayList<Player> getDiet(){
		return diet;
	}

	public String getType(){
		return this.type;
	}

	public boolean canEat(Player enemy){
		if (this.getType().equals("Monster")) {
			if (!(enemy.isDead())) {
				return false;
			}
			else {
				return true;
			}
		}
		else {
			if (!(enemy.getType().equals("Monster"))) {
				return false;
			}
			else {
				if (!(enemy.isBurned())) {
					return false;
				}
				else {
					return true;
				}
			}
		}
	}

}
//  write Player Class here
