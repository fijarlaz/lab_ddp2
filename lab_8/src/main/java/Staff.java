
public class Staff extends Manager {

	private String nama;
	private float gaji;
	public static float thresold;

	public Staff(String nama, float gaji) {
    	super(nama, gaji);
    	this.thresold = 18000;
    }

    public Staff(String nama, float gaji, float thresold) {
    	super(nama, gaji);
    	this.thresold = thresold;
    }
}
