package Karyawan;
import java.util.ArrayList;
public abstract class Karyawan{
	private String nama;
	private float gaji;
	private ArrayList<Karyawan> bawahan = new ArrayList<Karyawan>();	

    public Karyawan(String nama, float gaji) {
    	this.nama =  nama;
    	this.gaji = gaji;
    }

    abstract void Gajian();

    abstract int terGaji();

    public String getNama(){
    	return nama;
    }

    public float getGaji(){
    	return gaji;
    }

    public void setGaji(float gaji){
    	this.gaji = gaji;
    }

    public ArrayList<Karyawan> getBawahan(){
    	return bawahan;
    }

    public boolean canBeBoss(Karyawan bawahan){
    	if (bawahan.getClass().getSimpleName().equals("Manager")) {
    		return false;
    	}
    	else if (this.getClass().getSimpleName().equals("Manager")) {
    		return true;
    	}
    	else if (this.getClass().getSimpleName().equals("Staff")) {
    		if (bawahan.getClass().getSimpleName().equals("Manager") || bawahan.getClass().getSimpleName().equals("Staff")) {
    			return false;
    		}
    		return true;
    	}
    	return false;
    }

    public void addKacung(Karyawan kacung){
        this.bawahan.add(kacung);
    }
}