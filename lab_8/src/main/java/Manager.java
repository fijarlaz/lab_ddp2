public class Manager extends Karyawan {
	private String nama;
	private float gaji;
	private int banyakDiGaji=0;

	public Manager(String nama, float gaji) {
    	super(nama, gaji);
    }

    public String status(){
    	return nama +" " + gaji;
    }

    public void Gajian(){
    	this.banyakDiGaji++;
    }

    public int terGaji(){
    	return banyakDiGaji;
    }
}
