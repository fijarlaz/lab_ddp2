import java.util.ArrayList;
public class Intern extends Karyawan {
	private String nama;
	private float gaji;
	private int banyakDiGaji =0;

	public Intern(String nama, float gaji) {
    	super(nama, gaji);
    }

    public void Gajian(){
    	this.banyakDiGaji++;
    }

    public int terGaji(){
    	return banyakDiGaji;
    }
}
