package Office;
import java.util.ArrayList;
import Karyawan;
import Intern;

public class Office{
	public static ArrayList<Karyawan> pegawai = new ArrayList<Karyawan>();
	int counterGaji = 0;

	public Karyawan find(String name){
        for (Karyawan karyawan: pegawai) {
            if (karyawan.getNama().toUpperCase().equals(name)){
                return karyawan;
            }
        }
        return null;
    }

    public String add(String nama, String tipe, float gaji){
    	if (find(nama) !=null) {
            return "Sudah ada pegawai bernama "+nama;
        }
        if ("MANAGER".equals(tipe)) {
            Karyawan manager = new Manager(nama, gaji);
            pegawai.add(manager);
        }
        else if (tipe.equals("STAFF")) {
        	if (Lab8.thresold != 0) {
        		Karyawan staff = new Staff(nama, gaji, Lab8.thresold);
            	pegawai.add(staff);	
        	}
        	else {
	            Karyawan staff = new Staff(nama, gaji);
	            pegawai.add(staff);
        	}
        }
        else if (tipe.equals("INTERN")) {
            Karyawan intern = new Intern(nama,gaji);
            pegawai.add(intern);
        }
        return nama +" mulai bekerja sebagai "+tipe +" di PT. TAMPAN";
    }

    public void gajian(){
    	System.out.println("Semua karyawan telah diberikan gaji");
    	for (Karyawan orang : pegawai) {
    		orang.Gajian();
    		if (orang.terGaji() % 6 ==0) {
    			float gajiBaru = orang.getGaji() + (orang.getGaji() * 0.1F);
    			System.out.println(orang.getNama()+" mengalami kenaikan gaji sebesar 10% dari "+orang.getGaji()+ " menjadi "+gajiBaru);
    			orang.setGaji(gajiBaru);
    			if (orang.getClass().getSimpleName().equals("Staff") && orang.getGaji()>=Lab8.thresold) {
    				promoteStaff(orang);
    			}
    		}
    	}
    }


    public String status(String nama){
    	Karyawan p1 = find(nama);
    	if (p1 == null) {
    		return "Karyawan tidak ada";
    	}
    	return p1.getNama()+" "+p1.getGaji();
    }

    public String tambahBawahan(String atasan, String bawahan){
    	Karyawan p1 = find(atasan);
    	Karyawan p2 = find(bawahan);
    	if ((p1 != null) && (p2!=null)) {
    		if (p1.canBeBoss(p2)) {
    			p1.addKacung(p2);
    			return p2.getNama()+" berhasil ditambahkan menjadi bawahan " +p1.getNama(); 
    		}
    		return "Anda tidak berhak punya bawahan";
    	}
    	return "Nama tidak berhasil ditemukan";
    }

    public void promoteStaff(Karyawan staff){
        hapusBawahan(staff);
    	staff = new Manager(staff.getNama(), staff.getGaji());
    	System.out.println("Selamat, " + staff.getNama() +" berhasil dipromosikan menjadi manager");
        
    }

    public String cetakBawahan(String nama){
        Karyawan p1 = find(nama);
        String bawahan ="";
        if (p1.getClass().getSimpleName().equals("Intern")) {
            return "Intern tidak bisa punya bawahan";
        }
        else if (p1.getBawahan() != null) {
            for (Karyawan kacung : p1.getBawahan()) {
                bawahan+= kacung.getNama()+", ";
            }
            return "Bawahan " + nama +": "+bawahan.substring(0,bawahan.length()-2);
        }
        return nama+"tidak punya bawahan";
    }

    public void hapusBawahan(Karyawan staff){
        for (Karyawan karyawan : pegawai) {
            if (karyawan.getBawahan().contains(staff)) {
                karyawan.getBawahan().remove(staff)
            }
        }
    }
}