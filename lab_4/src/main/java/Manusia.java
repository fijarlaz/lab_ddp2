public class Manusia{
	private String nama;
	private int umur;
	private int uang;
	private float kebahagiaan;

	public Manusia(String nama, int umur, int uang){
		this.nama = nama;
		this.umur = umur;
		this.uang = uang;
		this.kebahagiaan = 50;
	}
	public Manusia(String nama, int umur){
		this.nama = nama;
		this.umur = umur;
		this.uang = 50000;
		this.kebahagiaan = 50;
	}

	public String getNama(){
		return nama;
	}

	public void setNama(String nama){
		this.nama = nama;
	}

	public int getUmur(){
		return umur;
	}

	public void setUmur(int umur){
		this.umur = umur;
	}

	public int getUang(){
		return uang;
	}

	public void setUang(int uang){
		this.uang = uang;
	}

	public void setKebahagiaan(float kebahagiaan){
		this.kebahagiaan = kebahagiaan;
	}
	public float getKebahagiaan(){
		return kebahagiaan;
	}

	public void beriUang(Manusia penerima){
		int jumlah = 0;
		for(int i = 0; i< penerima.nama.length(); i++){
			jumlah += (int)penerima.nama.charAt(i);
		}
		int uangDikirim = jumlah * 100;
		if (this.uang > uangDikirim){
			float penambahKebahagiaan = uangDikirim / 6000F;
			float jumlahBahagiaPengirim = this.getKebahagiaan() + penambahKebahagiaan;
			float jumlahBahagiaPenerima = penerima.getKebahagiaan() + penambahKebahagiaan;
			if (jumlahBahagiaPengirim < 100F) this.setKebahagiaan(jumlahBahagiaPengirim);
			else this.setKebahagiaan(100F);
			if(jumlahBahagiaPenerima <100F) penerima.setKebahagiaan(jumlahBahagiaPenerima);
			System.out.println(this.nama + " memberi uang sebanyak "+ uangDikirim + " kepada " + penerima.nama +", mereka berdua senang :D");
			this.uang -= uangDikirim;
			penerima.uang += uangDikirim;
		}
		else{
			System.out.println(this.nama + " ingin memberi uang kepada " + penerima.nama +" namum tidak memiliki cukup uang");
		}
	}

	public void beriUang(Manusia penerima, int uangDikirim){
		if (this.uang > uangDikirim){
			float penambahKebahagiaan = uangDikirim / 6000F;
			if (this.getKebahagiaan() + penambahKebahagiaan < 100F)this.setKebahagiaan(this.getKebahagiaan() + penambahKebahagiaan);
			else {this.setKebahagiaan(100F);}
			if (penerima.getKebahagiaan() + penambahKebahagiaan < 100F)penerima.setKebahagiaan(penerima.getKebahagiaan() + penambahKebahagiaan);
			else penerima.setKebahagiaan(100F);
			System.out.println(this.nama + " memberi uang sebanyak "+ uangDikirim + " kepada " + penerima.nama +", mereka berdua senang :D");
			this.setUang(this.getUang() - uangDikirim);
			penerima.setUang(penerima.getUang() + uangDikirim);
		}
		else{
			System.out.println(this.nama + " ingin memberi uang kepada " + penerima.nama +" namum tidak memiliki cukup uang");
		}

	}

	public void bekerja(int durasi, int bebanKerja){
		//to do
		if(this.umur < 18) System.out.println(this.nama +" belum boleh bekerja karena masih dibawah umur D:");
		else{
			int  pendapatan = 0;
			int bebanKerjaTotal = durasi * bebanKerja;
			if (bebanKerjaTotal <= this.kebahagiaan){
				this.kebahagiaan -= bebanKerjaTotal;
				pendapatan = bebanKerjaTotal * 10000;
				System.out.println(this.nama + " bekerja full time, total pendapatan : " +pendapatan);
			}
			else if(bebanKerjaTotal > this.getKebahagiaan()){
				int durasiBaru = (int)this.kebahagiaan / bebanKerja;
				int bebanKerjaBaru = durasiBaru * bebanKerja;
				pendapatan = bebanKerjaBaru * 10000;
				this.kebahagiaan -= bebanKerjaBaru;
				System.out.println(this.nama + " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan: " + pendapatan);
			}
			this.setUang(this.getUang()+pendapatan);
		}
	}

	public void rekreasi(String namaTempat){
		int biaya = namaTempat.length() * 10000;
		float jumlahBahagia = this.getKebahagiaan() + (float)namaTempat.length();
		if(this.getUang() > biaya){
			if(jumlahBahagia < 100F) this.setKebahagiaan(jumlahBahagia);
			else this.setKebahagiaan(100F);
			System.out.println(this.getNama() +" berekreasi di "+namaTempat+", "+this.getNama() +" senang :)");
			this.setUang(this.getUang() - biaya);
		}
		else System.out.println(this.getNama() +" tidak mempunyai cukup uang untuk berekreasi di "+namaTempat);
	}

	public void sakit(String namaPenyakit){
		float nilaiBahagia = this.getKebahagiaan() - namaPenyakit.length();
		if (nilaiBahagia >= 0) this.setKebahagiaan(nilaiBahagia);
		else this.setKebahagiaan(0F);
		System.out.println(this.getNama() +" terkena penyakit "+ namaPenyakit + " :O");
	}

	public String toString(){
		String awi = "Nama\t\t: " + this.getNama() + "\nUmur\t\t: "
		 + this.getUmur() + "\nUang\t\t: " + this.getUang() +"\nKebahagiaan\t: "
		 + this.getKebahagiaan();
		return awi;// to do 
	}
}
