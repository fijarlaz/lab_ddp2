package customer;
import movie.*;
import ticket.*;
import theater.*;
import java.util.Arrays;
import java.util.ArrayList;

public class Customer{
    private String name;
    private String kelamin;
    private int umur;

    public Customer(String name, String kelamin, int umur){
        this.name = name;
        this.kelamin = kelamin;
        this.umur = umur;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setKelamin(String kelamin){
        this.kelamin = kelamin;
    }

    public String getKelamin(){
        return kelamin;
    }

    public void setUmur(int umur){
        this.umur = umur;
    }

    public int getUmur(){
        return umur;
    }

    public Ticket orderTicket(Theater bioskop, String judul, String hari, String jenis){
        Ticket ticket = null;
        boolean jenisFilm = true;
        boolean filmAda = false;
        if (jenis == "Biasa") {
            jenisFilm = false;
        }
        for (int i=0;i< bioskop.getTickets().size() ;i++ ) {
            if (bioskop.getTickets().get(i).getHari().equals(hari) &&
                bioskop.getTickets().get(i).getFilm().getJudul().equals(judul) &&
                bioskop.getTickets().get(i).getTersedia()==jenisFilm) {
                filmAda = true;
                if (this.getUmur() < bioskop.getTickets().get(i).getFilm().getUmurMinimum()) {
                    System.out.println(this.getName() + " masih belum cukup umur untuk menonton "+ judul+" dengan rating "+bioskop.getTickets().get(i).getFilm().getRating());
                }
                else {
                    ticket = bioskop.getTickets().get(i);
                    System.out.println(this.getName()+" telah membeli tiket "+judul + " jenis "+ jenis+ " di "+bioskop.getNamaBioskop()+" pada hari "+hari+ " seharga Rp. "+bioskop.getTickets().get(i).getHarga());
                    bioskop.setKas(bioskop.getKas(), bioskop.getTickets().get(i).getHarga());
                    bioskop.getTickets().remove(i);
                }
            }
        }
        if (filmAda == false) {
            System.out.println("Tiket untuk film "+judul+" jenis "+jenis+" dengan jadwal "+hari +" tidak tersedia di "+bioskop.getNamaBioskop());
        }
        return ticket;
    }

    public void findMovie(Theater bioskop, String judul){
        for (int i = 0;i< bioskop.getFilms().length;i++ ) {
            if (bioskop.getFilms()[i].getJudul().equals(judul)) {
                System.out.println("------------------------------------------------------------------");
                System.out.println("Judul   : " + judul);
                System.out.println("Genre   : "+ bioskop.getFilms()[i].getGenre());
                System.out.println("Durasi  : "+ bioskop.getFilms()[i].getDurasi());
                System.out.println("Rating  : "+ bioskop.getFilms()[i].getRating());
                System.out.println("Jenis   : Film "+ bioskop.getFilms()[i].getJenis());
                System.out.println("------------------------------------------------------------------");
                break;
            }
        }
        System.out.println("Film "+judul+ " yang dicari "+name +" tidak ada di bioskop "+bioskop.getNamaBioskop());
    }


}
