package theater;
import customer.*;
import movie.*;
import ticket.*;
import java.util.ArrayList;
import java.util.Arrays;

public class Theater{

    private String namaBioskop;
    private ArrayList<Ticket> tickets;
    private Movie[] films;
    private int pendapatan;
    private int kas;

    public Theater(String namaBioskop,int kas, ArrayList<Ticket> tickets, Movie[] films){
        this.namaBioskop = namaBioskop;
        this.films = films;
        this.tickets = tickets;
        this.kas = kas;
    }

    public void setNamaBioskop(String namaBioskop){
        this.namaBioskop = namaBioskop;
    }

    public String getNamaBioskop(){
        return namaBioskop;
    }

    public void setKas(int kas, int pendapatan){
        this.kas = kas + pendapatan;
    }

    public int getKas(){
        return kas;
    }

    public ArrayList<Ticket> getTickets(){
        return tickets;
    }

    public void addTicket(Ticket tickets){
        this.tickets.add(tickets);
    }


    public void setFilms(Movie[] films){
        this.films = films;
    }

    public Movie[] getFilms(){
        return films;
    }

    public void printInfo(){
        System.out.print("------------------------------------------------------------------\n"+
                            "Bioskop                 : " + namaBioskop +
                            "\nSaldo Kas               : " + kas +
                            "\nJumlah tiket tersedia   : " + tickets.size()+
                            "\nDaftar Film tersedia    : "); //TAMBAH DAFTAR FILM YA
        for (int i=0;i< films.length ;i++ ) {
            if (i!=films.length-1) {
                System.out.print(films[i].getJudul()+", ");
            }
            else {
                System.out.println(films[i].getJudul());
            }
        }
        System.out.println("------------------------------------------------------------------");
    }

    public static void printTotalRevenueEarned(Theater[] theaters){
        int totalPendapatan = 0;
        for (int i=0;i<theaters.length ;i++ ) {
            totalPendapatan += theaters[i].getKas();
        }
        System.out.println("Total uang yang dimiliki Koh Mas : Rp." + totalPendapatan+
                            "\n------------------------------------------------------------------");
        for (int j=0;j<theaters.length ;j++ ) {
            System.out.println("Bioskop     : "+theaters[j].namaBioskop+
                                "\nSaldo Kas   : Rp. "+theaters[j].kas+"\n");
        }
        System.out.println("------------------------------------------------------------------");
    }

    public void filterMovieByGenre(String genre){
        String allMovie = "";

        for (Movie film: this.films ) {
            if (film.getGenre().contains(genre)) {
                // 
            }
        }
        System.out.println(allMovie.substring(0,allMovie.length()-1));
    }

}
