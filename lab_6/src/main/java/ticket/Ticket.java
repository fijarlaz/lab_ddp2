package ticket;
import customer.*;
import movie.*;
import theater.*;

public class Ticket{

    private Movie film;
    private String hari;
    private boolean tersedia;
    private int hargaTiket;
    public static String threeD ="";

    public Ticket(Movie film, String hari, boolean tersedia){
        this.film = film;
        this.hari = hari;
        this.tersedia = tersedia;
    }


    public void setFilm(Movie film){
        this.film = film;
    }

    public Movie getFilm(){
        return film;
    }

    public void setHari(String hari){
        this.hari = hari;
    }

    public String getHari(){
        return hari;
    }

    public void setTersedia(boolean tersedia){
        this.tersedia = tersedia;
    }

    public boolean getTersedia(){
        return tersedia;
    }

    public void setHarga(int hargaTiket){
        this.hargaTiket = hargaTiket;
    }

    public int getHarga(){
        if (hari == "Sabtu" || hari == "Minggu") {
            hargaTiket = 100000;
        }
        else {
            hargaTiket = 60000;
        }
        if (tersedia) {
            return (int)(hargaTiket + (int)hargaTiket * 0.2);
        }
        return hargaTiket;
    }

    public void printInfo(){
        System.out.println("------------------------------------------------------------------\n"+
                            "Film\t\t\t: " + this.film.getJudul()+
                            "\nJadwal Tayang\t: "+ this.hari +
                            "\nJenis\t\t\t: " + threeD);
    }

}
