package movie;
import ticket.*;
import theater.*;
public class Movie{

    private String judul;
    private String rating;
    private int durasi;
    private String genre;
    private String jenis;


    public Movie(String judul, String rating, int durasi, String genre, String jenis){
        this.judul = judul;
        this.rating = rating;
        this.durasi = durasi;
        this.genre = genre;
        this.jenis = jenis;
    }

    public int getUmurMinimum(){
        if (this.rating.toLowerCase().equals("remaja")) {
            return 13;
        }
        else if (this.rating.toLowerCase().equals("dewasa")) {
            return 17;
        }
        return 0;
    }

    public void setJudul(String judul){
        this.judul = judul;
    }

    public String getJudul(){
        return judul;
    }

    public void setRating(String rating){
        this.rating = rating;
    }

    public String getRating(){
        return rating;
    }

    public void setDurasi(int durasi){
        this.durasi = durasi;
    }

    public int getDurasi(){
        return durasi;
    }

    public void setGenre(String genre){
        this.genre = genre;
    }

    public String getGenre(){
        return genre;
    }

    public void setJenis(String jenis){
        this.jenis = jenis;
    }

    public String getJenis(){
        return jenis;
    }


    
}
