import java.util.Scanner;
public class RabbitHouse{
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String kasus = input.next();
		String namaKelinci = input.next();
		if (kasus.equals("normal")) System.out.println(hitungKelinci(namaKelinci.length()));
		else if (kasus.equals("palindrom")) System.out.println();
	}


	public static int hitungKelinci(int nama){
		if (nama == 1) return 1; // untuk kasus normal, bonusnya masih dipikirkan :v
		return (1 + nama * (hitungKelinci(nama-1))); // ketemu rumus f(n) = 1 + 2* f(n-1), untuk n>1
	}

	public static boolean isPalindrom(String kata){
		for(int i=0;i<kata.length()/2;i++){
			if (kata.charAt(i) != kata.charAt(kata.length()-1-i)) return false;
		}
		return true;
	}

}