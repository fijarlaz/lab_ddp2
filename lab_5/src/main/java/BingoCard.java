<<<<<<< HEAD:lab_5/src/main/java/BingoCard.java
/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Anda diwajibkan untuk mengimplementasikan method yang masih kosong
 * Anda diperbolehkan untuk menambahkan method jika dibutuhkan
 * HINT : Bagaimana caranya cek apakah sudah menang atau tidak? Mungkin dibutuhkan method yang bisa membantu? Hmmmm.
 * Semangat ya :]
 */
import java.util.Scanner;

public class BingoCard{
    private Number[][] numbers;
    private Number[] numberStates;
    private boolean isBingo;


    public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        Number[][] numbers = new Number[5][5];


        for(int i = 0; i < 5; i++){
            String[] baris = input.nextLine().split(" ");
            int j = 0;
            for (String angka : baris){
                numbers[i][j] = new Number(Integer.parseInt(angka), i, j);
                j++;
            }
        }

        Number[] states = new Number[100];
        for(int i=0; i<5; i++){
            for (int j=0; j<5; j++){;
              states[numbers[i][j].getValue()] = numbers[i][j];
            }
        }


        BingoCard card = new BingoCard(numbers,states);
        while (!card.isBingo()){
            String[] perintah = input.nextLine().split(" ");
            if (perintah[0].toLowerCase().equals("mark")){
                System.out.println(card.markNum(Integer.parseInt(perintah[1])));
            }
            else if(perintah[0].toLowerCase().equals("info")){
                System.out.println(card.info());
            }
            else if (perintah[0].toLowerCase().equals("restart")){
                card.restart();
            }
            else{
                System.out.println("Incorrect Command");
            }
        }
    }


	public Number[][] getNumbers() {
    return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

    public String markNum(int num){
        String out = "";
        try{
            if (numberStates[num].isChecked()) {
              out = num + " sebelumnya sudah tersilang";
            }
            else {
              numberStates[num].setChecked(true); out = num + " tersilang";
            }
        }
        catch(Exception e){
            out = "Kartu tidak memiliki angka " + num;
        }
        this.checkWinning();
        return out;
    }

    public String info(){
        String papan = "";
        for (Number[] number : numbers){
            for (Number num : number){
                if (!numberStates[num.getValue()].isChecked()){
                    papan += "| " +numberStates[num.getValue()].getValue() + " ";
                }
                else{
                    papan += "| X  ";
                }
            }
            papan += "|\n";
        }
        return papan.substring(0,papan.length()-1);
    }


	public void restart(){
		for (Number[] number : numbers){
      		for (Number num : number){
        		numberStates[num.getValue()].setChecked(false);
      		}
    	}
      setBingo(false);
		System.out.println("Mulligan!");
	}


	public void checkWinning(){ //UBAH INI CUY
    if (this.checkWinVertical() || this.checkWinHorizontal() ||
        this.checkWinDiagonal1() || this.checkWinDiagonal2()) {
      this.setBingo(true);
      System.out.println(this.info());
    }

	}

  public boolean checkWinHorizontal(){
    for (int i=0; i<5; i++){
      int counter =0;
      for (int j=0;j<5 ;j++ ) {
        if (numberStates[numbers[i][j].getValue()].isChecked()) counter++;
      }
        if (counter ==5){
          return true;
        }
      }
      return false;
  }

  public boolean checkWinVertical(){
    for (int i =0; i<5 ;i++ ){
      int counter = 0;
      for (int j=0;j<5 ;j++ ) {
        if (numberStates[numbers[j][i].getValue()].isChecked()) {
          counter++;
        }
      }
        if(counter ==5){
          return true;
        }
      }
      return false;
  }

  public boolean checkWinDiagonal1(){
    int counter =0;
    for (int i=0;i<5 ;i++ ){
      if (numberStates[numbers[i][i].getValue()].isChecked()) counter++;
      if (counter ==5) {
        return true;
        }
    }
    return false;
  }

  public boolean checkWinDiagonal2(){
    int counter =0;
    for (int i=0;i<5 ;i++ ) {
      for (int j=4;j>=0 ;j-- ) {
        if (numberStates[numbers[i][j].getValue()].isChecked()) {
          counter++;
        }
        if (counter ==5) {
          return true;
        }
      }
    }
    return false;
  }

}
=======
/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */

public class BingoCard {

	private Number[][] numbers;
	private Number[] numberStates; 
	private boolean isBingo;
	
	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}	

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public String markNum(int num){
		//TODO Implement
		return "hehe";
	}	
	
	public String info(){
		//TODO Implement
		return "hehe";
	}
	
	public void restart(){
		//TODO Implement
		System.out.println("Mulligan!");
	}
	

}
>>>>>>> 0d65ee7b6b68537950414ed5d4416d21e131fcce:lab_instructions/lab_5/BingoCard.java
