import java.util.Scanner;

public class Lab5DDP2 {
		
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		String[] input;
		
		Number[][] numbers = new Number[5][5];
		Number[] states = new Number[100];
		
		for(int i=0; i<5; i++){
			input = sc.nextLine().split(" ");
			for(int j=0; j<5; j++){
				Number num = new Number((Integer.parseInt(input[j])),i,j);
				numbers[i][j] = num;
				states[num.getValue()] = num;
			}
		}
		
		BingoCard card = new BingoCard(numbers,states);
		
		while(!card.isBingo()){
			input = sc.nextLine().split(" ");
			switch(input[0].toLowerCase()){
			case "mark":
				int num = Integer.parseInt(input[1]);
				System.out.println(card.markNum(num));				
				break;
				
			case "info":
				System.out.println(card.info());
				break;
				
			case "restart":
				card.restart();
				break;
				
			default:
				System.out.println("Incorrect command");
			}
		}
		System.out.println("BINGO!");
		System.out.println(card.info());
		
		sc.close();		
		
	}

}
