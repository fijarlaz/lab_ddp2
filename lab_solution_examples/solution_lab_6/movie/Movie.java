package movie;

public class Movie {
    private String title;
    private String parentalRating;
    private int duration;
    private String genre;
    private boolean isLocal;

    public Movie(String title, String parentalRating, int duration, String genre, String localStatus) {
        this.title = title;
        this.parentalRating = parentalRating;
        this.duration = duration;
        this.genre = genre;
        this.isLocal = (localStatus.equalsIgnoreCase("Lokal")) ? true : false;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getParentalRating() {
        return parentalRating;
    }

    public void setParentalRating(String parentalRating) {
        this.parentalRating = parentalRating;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public boolean isLocal() {
        return isLocal;
    }

    public void setLocal(boolean local) {
        isLocal = local;
    }

    @Override
    public String toString() {
        return "------------------------------------------------------------------" +
                "\nJudul\t: " + this.title +
                "\nGenre\t: " + this.genre +
                "\nDurasi\t: " + this.duration +
                " menit\nRating\t: " + this.parentalRating +
                "\nJenis\t: " + ((isLocal)? "Film Lokal" : "Film Import") +
                "\n------------------------------------------------------------------";
    }
    //BONUS
    public boolean equals(Movie other) {
        return this.title.equals(other.title) && this.genre.equals(other.genre);
    }
}
