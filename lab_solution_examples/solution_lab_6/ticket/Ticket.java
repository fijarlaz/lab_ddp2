package ticket;

import movie.Movie;

public class Ticket {
    private Movie film;
    private String schedule;
    private boolean is3D;
    private boolean isWatched;
    private static final int DEFAULT_PRICE = 60000;
    private int price;

    public Ticket(Movie film, String schedule, boolean is3D) {
        this.film = film;
        this.schedule = schedule;
        this.is3D = is3D;
        this.isWatched = false;
        this.price = calculateTicketPrice();
    }

    public Ticket(Movie film, String schedule) {
        this(film, schedule, false);
    }

    public Movie getFilm() {
        return film;
    }

    public void setFilm(Movie film) {
        this.film = film;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public boolean is3D() {
        return is3D;
    }

    public void set3D(boolean is3D) {
        this.is3D = is3D;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isWatched() {
        return isWatched;
    }

    public void setWatched(boolean watched) {
        isWatched = watched;
    }

    public int calculateTicketPrice() {
        int price = DEFAULT_PRICE;
        if (schedule.equalsIgnoreCase("Sabtu") || schedule.equalsIgnoreCase("Minggu"))
            price += 40000;
        return is3D ? (int) (price * 1.2) : price;
    }

    public String printTicket() {
        return "Film\t: " + film.getTitle() +
                "\nJadwal Tayang\t: " + schedule +
                "\nJenis\t: " + (is3D ? "3 Dimensi" : "Biasa");
    }
}
