package customer;

import movie.Movie;
import theater.Theater;
import ticket.Ticket;

import java.util.ArrayList;

public class Customer {
    private String name;
    private boolean isFemale;// false -> Pria, True-> Wanita
    private int age;
    private ArrayList<Ticket> ticketHistory; //BONUS

    public Customer(String name, String gender, int age) {
        this.name = name;
        this.isFemale = gender.equals("Perempuan") ? true : false;
        this.age = age;
        this.ticketHistory = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isFemale() {
        return isFemale;
    }

    public void setFemale(boolean isFemale) {
        this.isFemale = isFemale;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    //standar
    public Ticket orderTicket(Theater bioskop, String namaFilm, String jadwal, String tigaDimensi) {
        Ticket ticket = bioskop.findTicket(namaFilm, jadwal, tigaDimensi);
        if (ticket != null) {
            if (!ageIsValid(bioskop, namaFilm)) {
                System.out.println(this.name + " masih belum cukup umur untuk menonton " + namaFilm + " dengan rating " + ticket.getFilm().getParentalRating());
            } else {
                bioskop.purchaseTicket(ticket);
                ticketHistory.add(ticket);
                System.out.println(this.name + " telah membeli tiket " + namaFilm +
                        " jenis " + tigaDimensi + " di " + bioskop.getLocationName() + " pada hari " + jadwal +
                        " seharga Rp. " + ticket.getPrice());
            }
        } else {
            System.out.println("Tiket untuk film " + namaFilm + " jenis " + tigaDimensi +
                    " dengan jadwal " + jadwal + " tidak tersedia di " + bioskop.getLocationName());
        }
        return ticket;
    }

    private boolean ageIsValid(Theater bioskop, String namaFilm) {
        String parentalRating = bioskop.checkMovieDetail(namaFilm).getParentalRating();
        if ((parentalRating.equals("Remaja") && this.age < 13) || (parentalRating.equals("Dewasa") && this.age < 17)) {
            return false;
        }
        return true;
    }

    public void findMovie(Theater bioskop, String movieName) {
        Movie film = bioskop.checkMovieDetail(movieName);
        if (film == null) {
            System.out.println("Film " + movieName + " yang dicari " + this.name + " tidak ada di bioskop " + bioskop.getLocationName());
        } else {
            System.out.println(film.toString());
        }
    }

    //Bonus
    public void watchMovie(Ticket movieToWatch) {

        for (Ticket t : ticketHistory) {
            if (t == movieToWatch) {
                if (!t.isWatched()) {
                    System.out.println(this.name + " telah menonton film " + t.getFilm().getTitle());
                    t.setWatched(true);
                    return;
                } else {
                    System.out.println("Tiket" + t.getFilm().getTitle() + " sudah ditonton oleh " + this.name);
                    return;
                }
            }
        }
        System.out.println("Eh kok ga ketemu");
    }

    //Bonus
    public void cancelTicket(Theater bioskop) {
        Ticket latestMovieTicket = ticketHistory.get(ticketHistory.size() - 1);
        if (!latestMovieTicket.isWatched()) {
            if (bioskop.hasMovie(latestMovieTicket.getFilm())) {
                if (bioskop.getCashBalance() < latestMovieTicket.getPrice()) {
                    System.out.println("Maaf ya tiket tidak bisa dibatalkan, uang kas di bioskop " + bioskop.getLocationName() + " lagi tekor...");

                } else {
                    bioskop.refillTicket(latestMovieTicket);
                    bioskop.setCashBalance(bioskop.getCashBalance() - latestMovieTicket.getPrice());
                    Theater.totalRevenueEarned -= latestMovieTicket.getPrice();
                    ticketHistory.remove(latestMovieTicket);
                    System.out.println("Tiket film " + latestMovieTicket.getFilm().getTitle() + " dengan waktu tayang " + latestMovieTicket.getSchedule() +
                            " jenis " + (latestMovieTicket.is3D() ? "3 Dimensi" : "Biasa") + " dikembalikan ke bioskop " + bioskop.getLocationName());
                }

            } else {
                System.out.println("Maaf tiket tidak bisa dikembalikan, " + latestMovieTicket.getFilm().getTitle() + " tidak tersedia dalam " + bioskop.getLocationName());
            }
        } else {
            System.out.println("Tiket tidak bisa dikembalikan karena film " + latestMovieTicket.getFilm().getTitle()
                    + " sudah ditonton oleh " + this.name);
        }
    }
}