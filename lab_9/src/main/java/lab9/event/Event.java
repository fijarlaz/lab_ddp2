package lab9.event;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
* A class representing an event and its properties
*/
public class Event
{
    /** Name of event */
    private String name;
    private LocalDateTime timeStart;
    private LocalDateTime timeEnd;
    private String costPerHour;

    /**
     * constructor of event class
     * @param name
     * @param timeStart
     * @param timeEnd
     * @param costPerHour
     */
    public Event(String name, LocalDateTime timeStart, LocalDateTime timeEnd, String costPerHour){
        this.name = name;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.costPerHour = costPerHour;
    }

    /**
     * accessor for start time of an event
     * @return
     */
    public LocalDateTime getTimeStart(){
        return timeStart;
    }

    /**
     * accessor for end time of an event
     * @return
     */
    public LocalDateTime getTimeEnd(){
        return timeEnd;
    }

    /**
     * accessor for cost per hour
     * @return
     */
    public String getCostPerHour() { return costPerHour;}

    /**
    * Accessor for name field. 
    * @return name of this event instance
    */
    public String getName()
    {
        return this.name;
    }
    
    // TODO: Implement toString()

    /**
     * method to string to print information of the event
     * @return
     */
    public String toString(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy, HH:mm:ss");
        return this.name+ "\n"+
                "Waktu mulai: "+ timeStart.format(formatter)+ "\n" +
                "Waktu selesai: " + timeEnd.format(formatter)+"\n" +
                "Biaya kehadiran: "+costPerHour ;
    }
    
    // HINT: Implement a method to test if this event overlaps with another event
    //       (e.g. boolean overlapsWith(Event other)). This may (or may not) help
    //       with other parts of the implementation.
}
