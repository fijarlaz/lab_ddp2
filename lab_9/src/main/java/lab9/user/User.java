package lab9.user;

import lab9.event.Event;
import java.util.ArrayList;
import java.util.Comparator;
import java.math.BigInteger;

/**
* Class representing a user, willing to attend event(s)
*/
public class User
{
    /** Name of user */
    private String name;
    
    /** List of events this user plans to attend */
    private ArrayList<Event> events;

    private BigInteger totalCost = new BigInteger("0");

    
    /**
    * Constructor
    * Initializes a user object with given name and empty event list
    */
    public User(String name)
    {
        this.name = name;
        this.events = new ArrayList<>();
    }
    
    /**
    * Accessor for name field
    * @return name of this instance
    */
    public String getName() {
        return name;
    }
    
    /**
    * Adds a new event to this user's planned events, if not overlapping
    * with currently planned events.
    *
    * @return true if the event if successfully added, false otherwise
    */
    public boolean addEvent(Event newEvent)
    {
        if (events.contains(newEvent)) return false;
        events.add(newEvent);
        this.totalCost = totalCost.add(new BigInteger(newEvent.getCostPerHour()));
        return false;
    }



    /**
    * Returns the list of events this user plans to attend,
    * Sorted by their starting time.
    * Note: The list returned from this method is a copy of the actual
    *       events field, to avoid mutation from external sources
    *
    * @return list of events this user plans to attend
    */
    public ArrayList<Event> getEvents() {
        ArrayList<Event> awi =  new ArrayList<Event>();
        awi.addAll(this.events);
        awi.sort(new Comparator<Event>() {
            @Override
            public int compare(Event o1, Event o2) {
                return o1.getTimeStart().compareTo(o2.getTimeStart());
            }
        });
        return awi;
    }

    /**
     *
     * a method to return the total cost of user
     * @return
     */
    public String getTotalCost(){
        return totalCost.toString();
    }

    public boolean canAttendEvent(Event acara){
        for (Event myEvent: events ) {
            if (acara.getTimeEnd().isAfter(myEvent.getTimeStart()) && acara.getTimeStart().isBefore(myEvent.getTimeEnd())) return false;
        }
        return true;
    }
}
