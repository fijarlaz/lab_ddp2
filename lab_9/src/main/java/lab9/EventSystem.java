package lab9;

import lab9.event.Event;
import lab9.user.User;

import java.util.ArrayList;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
* Class representing event managing system
*/
public class EventSystem
{
    /**
    * List of events
    */
    private ArrayList<Event> events;
    
    /**
    * List of users
    */
    private ArrayList<User> users;
    
    /**
    * Constructor. Initializes events and users with empty lists.
    */
    public EventSystem()
    {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    /**
     * a method to check if a user with the name is already exist or not, if not, return null, therefore, return the User
     * @param name
     * @return
     */
    public User getUser(String name){
        for (User pengguna: users) {
            if (pengguna.getName().equalsIgnoreCase(name) ){
                return pengguna;
            }
        }
        return null;
    }

    /**
     * method to return event
     * @param eventName
     * @return
     */
    public Event findEvent(String eventName){
        for (Event acara: events) {
            if (acara.getName().equalsIgnoreCase(eventName)){
                return acara;
            }
        }
        return null;
    }

    public ArrayList<Event> getEvents() {
        return events;
    }

    /**
     * a method to check either the event is already exist or not
     * if it is, return the event, therefore, return null
     * @param eventName
     * @return
     */
    public String getEvent(String eventName){
        for (Event acara: events) {
            if (acara.getName().equalsIgnoreCase(eventName)){
                return acara.toString();
            }
        }
        return "Acara tidak ada";
    }
    /**
     * a method to make an event, if the event isn't exist yet, add them to events arraylist and tell if the event has added sucessfully
     * but if it already exist, tell if event with that name has exist
     * @param name
     * @param startTimeStr
     * @param endTimeStr
     * @param costPerHourStr
     * @return
     */
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr) {
        Event acara = findEvent(name);
        if (acara!= null) return "Event "+name+" sudah ada!";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss");
        LocalDateTime timeEventStart = LocalDateTime.parse(startTimeStr, formatter);
        LocalDateTime timeEventEnd = LocalDateTime.parse(endTimeStr, formatter);
        if (timeEventEnd.isBefore(timeEventStart)) return "Waktu yang diinputkan tidak valid!";
        Event event = new Event(name, timeEventStart, timeEventEnd, costPerHourStr);

        this.events.add(event);

        return "Event "+name+" berhasil ditambahkan!";
    }

    /**
     *method to add user if there isn't another user with the same name yet
     * @param name
     * @return
     */
    public String addUser(String name)
    {
        User user;
        user = getUser(name);
        if (user != null){
            return "User " +name+" sudah ada!";
        }
        User pengguna = new User(name);
        users.add(pengguna);
        return "User "+name+" berhasil ditambahkan!";
    }

    /**
     *method to register user to an event
     * @param userName
     * @param eventName
     * @return
     */
    public String registerToEvent(String userName, String eventName)
    {
        User user = getUser(userName);
        Event event = findEvent(eventName);
        if (user==null && event==null) return "Tidak ada pengguna dengan nama "+userName +" dan acara dengan nama "+eventName+"!";
        if (user == null ) return "Tidak ada pengguna dengan nama "+userName+"!";
        if (event==null ) return "Tidak ada acara dengan nama "+ eventName+"!";
        if (user.canAttendEvent(event) || user.getEvents()==null){
            user.addEvent(event);
            return userName+" berencana menghadiri "+eventName+"!";
        }
        return userName+ " sibuk sehingga tidak dapat menghadiri "+eventName+"!";
    }
}