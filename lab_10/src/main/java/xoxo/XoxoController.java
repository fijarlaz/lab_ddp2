package xoxo;

import xoxo.util.*;
import xoxo.key.*;
import xoxo.exceptions.*;
import xoxo.crypto.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JOptionPane;

/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <Fijar>
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() throws IOException {
        //TODO: Write your code for logic and everything here
        File file = new File("output.txt");
        
        gui.setDecryptFunction(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    FileWriter writer = new FileWriter(file, true);
                    String message = gui.getMessageText();
                    String key = gui.getKeyText();
                    String seedStr = gui.getSeedText();
                    int seed;
                    if(seedStr.equalsIgnoreCase("DEFAULT_SEED")) {
                        seed = HugKey.DEFAULT_SEED;
                    } else {
                        seed = Integer.parseInt(seedStr);
                    }
                    XoxoDecryption xd = new XoxoDecryption(key);
                    gui.appendLog(xd.decrypt(message, seed));
                    writer.write(xd.decrypt(message, seed) + "\n ");
                    writer.flush();
                }
                catch(ArithmeticException ex) {
                    JOptionPane.showMessageDialog(null, "No key!");
                }
                catch(NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Seed is invalid!");
                }
                catch(Exception ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
                
            }
        });
        gui.setEncryptFunction(new ActionListener(){
        
            @Override
            public void actionPerformed(ActionEvent e)  {
                try {
                    FileWriter writer = new FileWriter(file, true);
                    String message = gui.getMessageText();
                    String key = gui.getKeyText();
                    String seedStr = gui.getSeedText();
                    int seed;
                    if(seedStr.equalsIgnoreCase("DEFAULT_SEED")) {
                        seed = HugKey.DEFAULT_SEED;
                    } else {
                        seed = Integer.parseInt(seedStr);
                    }
                    KissKey kk = new KissKey(key);
                    XoxoEncryption xe = new XoxoEncryption(kk.getKeyString());
                    XoxoMessage xm = xe.encrypt(message);
                    gui.appendLog(xm.getEncryptedMessage());
                    writer.write(xm.getEncryptedMessage() + " \t " + xm.getHugKey().getKeyString() + "\n");
                    writer.flush();
                }
                catch(ArithmeticException ex) {
                    JOptionPane.showMessageDialog(null, "You don't have a key!");
                }
                catch(NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Your seed is invalid!");
                }
                catch(Exception ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
                    
            }
        });

        gui.setClearTextAreaFunction(new ActionListener(){
        
            @Override
            public void actionPerformed(ActionEvent e) {
                gui.clearTextArea();
            }
        });
    }

    //TODO: Create any methods that you want
}