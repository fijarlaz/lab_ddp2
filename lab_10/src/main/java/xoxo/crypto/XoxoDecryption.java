package xoxo.crypto;

import xoxo.exceptions.*;
import xoxo.key.*;

/**
 * This class is used to create a decryption instance
 * that can be used to decrypt an encrypted message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoDecryption {

    /**
     * A Hug Key string that is required to decrypt the message.
     */
    private String hugKeyString;

    /**
     * Class constructor with the given Hug Key string.
     */
    public XoxoDecryption(String hugKeyString) {
        this.hugKeyString = hugKeyString;
    }

    /**
     * Decrypts an encrypted message.
     * 
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @return The original message before it is encrypted.
     */
    public String decrypt(String encryptedMessage, int seed) throws SizeTooBigException, RangeExceededException {
        //TODO: Implement decryption algorithm
        final int length = encryptedMessage.length();
        if(length > 1250) {
            throw new SizeTooBigException("Your message's size is too big!'");
        }

        if(seed < HugKey.MIN_RANGE || seed > HugKey.MAX_RANGE) {
            throw new RangeExceededException("Seed's value out of range)");
        }

        String decryptedMessage = "";
        for(int i = 0; i < encryptedMessage.length(); i++) {
            int step1 = hugKeyString.charAt(i % hugKeyString.length()) ^ seed;
            int step2 = step1 - 'a';
            char chr = encryptedMessage.charAt(i);
            int value = chr ^ step2;
            decryptedMessage += (char) value;
        }
        return decryptedMessage;

        
    }
}