package xoxo.crypto;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;
import xoxo.key.HugKey;
import xoxo.key.KissKey;
import xoxo.util.XoxoMessage;

/**
 * This class is used to create an encryption instance
 * that can be used to encrypt a plain text message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoEncryption {

    /**
     * A Kiss Key object that is required to encrypt the message.
     */
    private KissKey kissKey;

    /**
     * Class constructor with the given Kiss Key
     * string to build the Kiss Key object.
     * 
     * @throws KeyTooLongException if the length of the
     *         kissKeyString exceeded 28 characters.
     */
    public XoxoEncryption(String kissKeyString) throws KeyTooLongException{
        this.kissKey = new KissKey(kissKeyString);
    }

    /**
     * Encrypts a message in order to make it unreadable.
     * 
     * @param message The message that wants to be encrypted.
     * @return A XoxoMessage object that contains the encrypted message
     *         and a Hug Key object that can be used to decrypt the message.
     */
    public XoxoMessage encrypt(String message) {
        String encryptedMessage = this.encryptMessage(message); 
        return new XoxoMessage(encryptedMessage, new HugKey(this.kissKey));
    }

    /**
     * Encrypts a message in order to make it unreadable.
     * 
     * @param message The message that wants to be encrypted.
     * @param seed A number to generate different Hug Key.
     * @return A XoxoMessage object that contains the encrypted message
     *         and a Hug Key object that can be used to decrypt the message.
     * @throws RangeExceededException if ... <complete this>
     */
    public XoxoMessage encrypt(String message, int seed) throws RangeExceededException {
        //TODO: throw RangeExceededException for seed requirements
        if(seed < HugKey.MIN_RANGE || seed > HugKey.MAX_RANGE) {
            throw new RangeExceededException("Seed value out of range (0 -- 36 allowed (inclusive))");
        } else {
            String encryptedMessage = this.encryptMessage(message); 
            return new XoxoMessage(encryptedMessage, new HugKey(this.kissKey, seed));
        }
        
    }

    /**
     * Runs the encryption algorithm to turn the message string
     * into an ecrypted message string.
     * 
     * @param message The message that wants to be encrypted.
     * @return The encrypted message string.
     * @throws SizeTooBigException if ... <complete this>
     * @throws InvalidCharacterException if ... <complete this>
     */
    public String encryptMessage(String message) throws SizeTooBigException, InvalidCharacterException {
        //TODO: throw SizeTooBigException for message requirements
        final int length = message.length();
        String encryptedMessage = "";
        Pattern p = Pattern.compile("[^[a-zA-Z@]]", Pattern.CASE_INSENSITIVE);
        Pattern q = Pattern.compile("[0-9]");
        Matcher c = p.matcher(this.kissKey.getKeyString());
        Matcher d = q.matcher(this.kissKey.getKeyString());
        boolean r = c.find();
        boolean s = d.find();
        
        
        if(length > 1250) {
            throw new SizeTooBigException("Your message's size is too big!");
        }

        if(r || s) {
            throw new InvalidCharacterException("Illegal character found!");
        }

        for (int i = 0; i < length; i++) {
            //TODO: throw InvalidCharacterException for message requirements
            int m = message.charAt(i);
            int k = this.kissKey.keyAt(i) - 'a';
            int value = m ^ k;
            encryptedMessage += (char) value;
        }
                   
        return encryptedMessage;
        
        
    }

}

