package xoxo;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextArea;

/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <Fijar>
 */
public class XoxoView {
    
    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    
    /**
     * A field to be the input of the seed.
     */
    private JTextField seedField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField; 

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    //TODO: You may add more components here

    private JButton clearTextAreaButton;

    private JFrame frame;

    private JPanel textFieldPanel;

    private JPanel buttonPanel;

    private JPanel textAreaPanel;


    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        //TODO: Construct your GUI here
        frame = new JFrame("Code Cracker");
        textFieldPanel = new JPanel(new FlowLayout());
        buttonPanel = new JPanel(new FlowLayout());
        textAreaPanel = new JPanel(new FlowLayout());
        encryptButton = new JButton("Encrypt");
        decryptButton = new JButton("Decrypt");
        clearTextAreaButton = new JButton("Clear");
        messageField = new JTextField("Message");
        messageField.setPreferredSize(new Dimension(150 ,25));
        keyField = new JTextField("Key");
        keyField.setPreferredSize(new Dimension(150 ,25));
        seedField = new JTextField("Seed");
        seedField.setPreferredSize(new Dimension(150 ,25));
        logField = new JTextArea();
        logField.setPreferredSize(new Dimension(450, 80));
        logField.setEditable(false);
        textFieldPanel.add(messageField);
        textFieldPanel.add(keyField);
        textFieldPanel.add(seedField);
        buttonPanel.add(encryptButton);
        buttonPanel.add(decryptButton);
        buttonPanel.add(clearTextAreaButton);
        textAreaPanel.add(logField);
        frame.setLayout(new BorderLayout());;
        frame.add(textFieldPanel, BorderLayout.NORTH);
        frame.add(buttonPanel, BorderLayout.CENTER);
        frame.add(textAreaPanel, BorderLayout.SOUTH);
        frame.setSize(500,200);
        frame.setResizable(false);
        frame.setVisible(true);
    }

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     * 
     * @return The input key string.
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    public void clearTextArea() {
        logField.setText("");
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }

    public void setClearTextAreaFunction(ActionListener listener) {
        clearTextAreaButton.addActionListener(listener);
    }
}